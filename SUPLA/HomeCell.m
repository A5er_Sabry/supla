//
//  HomeCell.m
//  SUPLA
//
//  Created by Ebrahim on 3/30/21.
//  Copyright © 2021 AC SOFTWARE SP. Z O.O. All rights reserved.
//

#import "HomeCell.h"
#import "MGSwipeButton.h"
#import "SAChannel+CoreDataClass.h"
#import "SAChannelGroup+CoreDataClass.h"
#import "SAChannelStateExtendedValue.h"
#import "SAChannelStatePopup.h"
#import "SAChannelCaptionEditor.h"
#import "SuplaApp.h"
#include "proto.h"


@implementation HomeCell{
    BOOL _initialized;
    BOOL _captionTouched;
    SAChannelBase *_channelBase;
    UITapGestureRecognizer *tapGr1;
    UITapGestureRecognizer *tapGr2;
    UILongPressGestureRecognizer *longPressGr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void) setShadow :(UIView*)view {
    [view.layer setShadowOffset:CGSizeMake(0, 0)];
    [view.layer setShadowColor:[self.picker.backgroundColor CGColor]];
    view.layer.cornerRadius = 5;
    view.layer.shadowRadius = 5;
    [view.layer setShadowOpacity:0.2];
}

-(void)setChannelBase:(SAChannelBase *)channelBase {
    //TODO: Add support for WINDSENSOR, PRESSURESENSOR, RAINSENSOR, WEIGHTSENSOR
    [self setShadow:self.pickerContainer];
    self.picker.layer.cornerRadius = 8;
    _channelBase = channelBase;
    BOOL isGroup = [channelBase isKindOfClass:[SAChannelGroup class]];
    self.channelStateIcon.hidden = YES;
    self.channelWarningIcon.hidden = YES;
    
    if ( isGroup ) {
        self.rightActiveStatus.percent = ((SAChannelGroup*)channelBase).activePercent;
        self.rightActiveStatus.singleColor = YES;
        self.rightActiveStatus.hidden = NO;
        self.rightOnLine.shapeType = stLinearVertical;
        self.leftOnline.shapeType = stLinearVertical;
    } else {
        self.rightActiveStatus.hidden = YES;
        self.rightOnLine.shapeType = stDot;
        self.leftOnline.shapeType = stDot;
        
        if ([channelBase isKindOfClass:[SAChannel class]]) {
            SAChannel *channel = (SAChannel*)channelBase;
            UIImage *stateIcon = channel.stateIcon;
            if (stateIcon) {
                self.channelStateIcon.hidden = NO;
                self.channelStateIcon.image = stateIcon;
            }
            
            UIImage *warningIcon = channel.warningIcon;
            if (warningIcon != nil) {
                self.channelWarningIcon.hidden = NO;
                self.channelWarningIcon.image = warningIcon;
            }
            
        }
    }
    
    self.rightOnLine.percent = [channelBase onlinePercent];
    self.rightOnLine.percent = self.rightActiveStatus.percent;
    


    [self.channelName setText:[channelBase getChannelCaption]];
    [self.channelImage setImage:[channelBase getIconWithIndex:0]];
//    [self.image2 setImage:[channelBase getIconWithIndex:1]];
    
    switch(channelBase.func) {
        case SUPLA_CHANNELFNC_CONTROLLINGTHEGATE:
        case SUPLA_CHANNELFNC_CONTROLLINGTHEGARAGEDOOR:
        case SUPLA_CHANNELFNC_CONTROLLINGTHEDOORLOCK:
        case SUPLA_CHANNELFNC_CONTROLLINGTHEGATEWAYLOCK:
        case SUPLA_CHANNELFNC_CONTROLLINGTHEROLLERSHUTTER:
        case SUPLA_CHANNELFNC_CONTROLLINGTHEROOFWINDOW:
        case SUPLA_CHANNELFNC_ELECTRICITY_METER:
        case SUPLA_CHANNELFNC_IC_ELECTRICITY_METER:
        case SUPLA_CHANNELFNC_IC_GAS_METER:
        case SUPLA_CHANNELFNC_IC_WATER_METER:
        case SUPLA_CHANNELFNC_IC_HEAT_METER:
        case SUPLA_CHANNELFNC_RGBLIGHTING:
        case SUPLA_CHANNELFNC_DIMMER:
        case SUPLA_CHANNELFNC_DIMMERANDRGBLIGHTING:
        case SUPLA_CHANNELFNC_THERMOSTAT_HEATPOL_HOMEPLUS:
        case SUPLA_CHANNELFNC_THERMOMETER:
        case SUPLA_CHANNELFNC_HUMIDITYANDTEMPERATURE:
        case SUPLA_CHANNELFNC_DIGIGLASS_HORIZONTAL:
        case SUPLA_CHANNELFNC_DIGIGLASS_VERTICAL:
            self.leftOnline.hidden = YES;
            self.rightOnLine.hidden = NO;
            break;
        case SUPLA_CHANNELFNC_POWERSWITCH:
        case SUPLA_CHANNELFNC_LIGHTSWITCH:
        case SUPLA_CHANNELFNC_STAIRCASETIMER:
        case SUPLA_CHANNELFNC_VALVE_OPENCLOSE:
            self.leftOnline.hidden = NO;
            self.rightOnLine.hidden = NO;
            break;
        case SUPLA_CHANNELFNC_NOLIQUIDSENSOR:
        case SUPLA_CHANNELFNC_OPENINGSENSOR_DOOR:
        case SUPLA_CHANNELFNC_OPENINGSENSOR_GARAGEDOOR:
        case SUPLA_CHANNELFNC_OPENINGSENSOR_GATE:
        case SUPLA_CHANNELFNC_OPENINGSENSOR_GATEWAY:
        case SUPLA_CHANNELFNC_OPENINGSENSOR_ROLLERSHUTTER:
        case SUPLA_CHANNELFNC_OPENINGSENSOR_ROOFWINDOW:
        case SUPLA_CHANNELFNC_OPENINGSENSOR_WINDOW:
        case SUPLA_CHANNELFNC_MAILSENSOR:
            self.leftOnline.hidden = NO;
            self.rightOnLine.hidden = NO;
            self.rightOnLine.shapeType = stRing;
            self.leftOnline.shapeType = stRing;
            break;
        default:
            self.leftOnline.hidden = YES;
            self.rightOnLine.hidden = YES;
            break;
    }
    
    
    if ( channelBase.func == SUPLA_CHANNELFNC_THERMOMETER ) {
        [self.temp setText:[[channelBase attrStringValue] string]];
    } else if ( channelBase.func== SUPLA_CHANNELFNC_HUMIDITYANDTEMPERATURE ) {
        
        [self.temp setText:[[channelBase attrStringValue] string]];
        [self.humidity setText:[[channelBase attrStringValueWithIndex:1 font:nil] string]];
       
    } else if ( channelBase.func == SUPLA_CHANNELFNC_DEPTHSENSOR
                 || channelBase.func == SUPLA_CHANNELFNC_WINDSENSOR
                 || channelBase.func == SUPLA_CHANNELFNC_WEIGHTSENSOR
                 || channelBase.func == SUPLA_CHANNELFNC_PRESSURESENSOR
                 || channelBase.func == SUPLA_CHANNELFNC_RAINSENSOR
                 || channelBase.func == SUPLA_CHANNELFNC_HUMIDITY ) {
        [self.measuredValue setText:[[channelBase attrStringValue] string]];
    } else if ( channelBase.func == SUPLA_CHANNELFNC_DISTANCESENSOR  ) {
        [self.distance setText:[[channelBase attrStringValue] string]];
    } else if ( channelBase.func == SUPLA_CHANNELFNC_ELECTRICITY_METER
                || channelBase.func == SUPLA_CHANNELFNC_IC_ELECTRICITY_METER
                || channelBase.func == SUPLA_CHANNELFNC_IC_WATER_METER
                || channelBase.func == SUPLA_CHANNELFNC_IC_GAS_METER
                || channelBase.func == SUPLA_CHANNELFNC_IC_HEAT_METER) {
        
        [self.measuredValue setText:[[channelBase attrStringValue] string]];
                
    } else if ( channelBase.func == SUPLA_CHANNELFNC_THERMOSTAT_HEATPOL_HOMEPLUS ) {
        [self.temp setAttributedText:[channelBase attrStringValueWithIndex:0 font:self.temp.font]];
    } else {
        if ( [channelBase isOnline] ) {
            NSLog(@"%@",@"is online");
        }
    }
    
}

@end

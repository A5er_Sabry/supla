//
//  HomeCell.h
//  SUPLA
//
//  Created by Ebrahim on 3/30/21.
//  Copyright © 2021 AC SOFTWARE SP. Z O.O. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAChannelBase+CoreDataClass.h"
#import "SAUIChannelStatus.h"
#import "UIHelper.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet SAUIChannelStatus *leftOnline;
@property (weak, nonatomic) IBOutlet SAUIChannelStatus *rightOnLine;
@property (weak, nonatomic) IBOutlet SAUIChannelStatus *rightActiveStatus;
@property (weak, nonatomic) IBOutlet UIImageView *channelStateIcon;
@property (weak, nonatomic) IBOutlet UIImageView *channelWarningIcon;
@property (weak, nonatomic) IBOutlet UILabel *channelName;
@property (weak, nonatomic) IBOutlet UIImageView *channelImage;
@property (weak, nonatomic) IBOutlet UIView *pickerContainer;
@property (weak, nonatomic) IBOutlet UIView *picker;

// مش عارف مش فاهم
@property (weak, nonatomic) IBOutlet UILabel *temp;
@property (weak, nonatomic) IBOutlet UILabel *humidity;
@property (weak, nonatomic) IBOutlet UILabel *distance;
@property (weak, nonatomic) IBOutlet UILabel *measuredValue;
@property (nonatomic) BOOL captionTouched;
@property (nonatomic) BOOL captionEditable;
-(void)setChannelBase:(SAChannelBase *)channelBase;
@end

NS_ASSUME_NONNULL_END
